package com.zuitt.activity;
import java.util.Date;

public class Course {
    private String name;
    private String description;
    private int seats;
    private Double fee;
    private String startDate;
    private String endDate;

    private User instructor;

    public Course(){
        this.instructor = new User();
    }

//    public Course(String name, String description, int seats, Double fee, String startDate, String endDate){
//        this.name = name;
//        this.description = description;
//        this.seats = seats;
//        this.fee = fee;
//        this.startDate = startDate;
//        this.endDate = endDate;
//    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getDescription(){
        return this.description;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public int getSeats(){
        return this.seats;
    }

    public void setSeats(int seats){
        this.seats = seats;
    }

    public double getFee(){
        return this.fee;
    }

    public void setFee(double fee){
        this.fee = fee;
    }

    public String getStartDate(){
        return this.startDate;
    }

    public void setStartDate(String startDate){
        this.startDate = startDate;
    }

    public String getEndDate(){
        return this.endDate;
    }

    public void setEndDate(String endDate){
        this.endDate = endDate;
    }

    public User getInstructor(){
        return instructor;
    }

    public void setInstructor(User instructor){
        this.instructor = instructor;
    }

    public String getInstructorName(){
        return this.instructor.getName();
    }
}
