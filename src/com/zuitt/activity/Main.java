package com.zuitt.activity;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        Course java = new Course();
        Course javaScript = new Course();

        User javaInstructor = new User("Monkey D. Luffy", 18, "gonnabekingofpirates@gmail.com","East Blue, New World");

        //javaInstructor.greetings();

        java.setName("JAVA");
        java.setDescription("Intro to JAVA");
        java.setSeats(30);
        java.setFee(5431.69);
        java.setStartDate("10-JUL-2023");
        java.setStartDate("14-JUL-2023");

        System.out.println("Hi! I am " + javaInstructor.getName() + ". I'm " + javaInstructor.getAge() + " years old. You can reach me via email: " + javaInstructor.getEmail() + ". When I'm off work, I can be found at my ship (Thousand Sunny) in " + javaInstructor.getAddress());

        System.out.println("Welcome to the course " + java.getName() + ". This course can be described as an " + java.getDescription() + " for career-shifters. Your instructor for this course is Captain " + javaInstructor.getName() + ". Enjoy!");
    }
}
